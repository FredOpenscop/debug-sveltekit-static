import type { PageServerLoad } from './$types';
import itemsFile from '../../../../data/pages/others.yaml'


export const load: PageServerLoad = ({ params }) => {
    const title = params.title.toLowerCase().replace('.html', '')
    const index = itemsFile.findIndex((item: any) => item.id.toLowerCase() === title)
    return {
        item: itemsFile[index]
    }
}