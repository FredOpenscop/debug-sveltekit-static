import type { PageServerLoad } from './$types';
import itemsFile from '../../data/pages/home.yaml'


export const load: PageServerLoad = ({ params }) => {
    return {
        item: itemsFile
    }
}