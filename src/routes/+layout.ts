import menuFile from '../data/menu.yaml'
import pages from '../data/pages/others.yaml'
import type { LayoutLoad } from './$types';

export const load: LayoutLoad = ({ url }) => {
    return {
        url: url.href,
        menuData: menuFile,
        pages
    }
}

export const prerender = true;